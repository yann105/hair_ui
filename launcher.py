import subprocess
from threading import Thread
from typing import List


class Launcher(Thread):
    """
    Launcher of script in a new thread.
    """

    def __init__(self, script_path: str, incidents: List[str] = None):
        Thread.__init__(self)
        self.script_path = script_path
        self.incidents = incidents

    def run(self):
            subprocess.call(f"{self.script_path} '{str(self.incidents)}'", shell=True)
