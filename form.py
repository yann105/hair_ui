import glob
from tkinter import messagebox
import matplotlib.pyplot as plt
from mongoengine import connect
import matplotlib.image as mpimg
from actions import display_card
from utils import *
from utils import generate_form


def on_closing():
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
      window.destroy()


def opensystem(self):
    x = lbox.curselection()[0]
    img = mpimg.imread(lbox.get(x))
    plt.imshow(img)
    plt.show()

connect(
    db='ocean_hackaton',
    host='mongodb://localhost:27017/'
)

window = Tk()
window.title("HAIR")
window.geometry('320x450')

frame = Frame(window, width=400, height=200, borderwidth=0)
frame.place(relx=0.5, rely=0.1, anchor=CENTER)

# label name = cle du dict!
form = generate_form(frame)

btn = ttk.Button(window, text="Valider", command=lambda: submit(form))
btn.place(relx=0.8, rely=0.3, anchor=S)

btn = ttk.Button(window, text="Afficher incidents", command=display_card)
btn.place(relx=0.25, rely=0.3, anchor=S)

flist = glob.glob("*_modelisation.png")

lbox = Listbox(window, height=15, width=35)
lbox.place(relx=0.5, rely=0.95, anchor=S)
lbox.bind("<Double-Button-1>", opensystem)

# THE ITEMS INSERTED WITH A LOOP
for item in flist:
    lbox.insert(END, item)

window.protocol("WM_DELETE_WINDOW", on_closing)
window.mainloop()