import geojson
from geojson import Point, GeometryCollection, Polygon, FeatureCollection, Feature

from incident import Incident, Displayer
from launcher import Launcher
import geojsonio


def submit(form):
    """
    This method is calling when a user clic on the submit button

    :param form: The list of (label, input)
    """
    data = {}
    errors = []
    for entry in form:
        key = Displayer.from_value(entry[0])
        value = entry[1].get()
        data[key] = value
        if not value:
            errors.append(key)
    if errors:
        raise Exception(f'Value is required for field(s) {errors}')
    Incident().from_dict(data).save()
    incidents = ' '.join([str(incident) for incident in Incident.objects()])
    Launcher("./matlab.sh", incidents).start()


def display_card():
    """
    open the browser and display a map with the location of all incidents
    """
    locations = [Feature(geometry=Point((l[0], l[1], l[0], l[0]))) for l in
                 Incident.objects.order_by('creation_date').values_list('location')]
    geojsonio.display(locations)


