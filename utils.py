from tkinter import ttk
from typing import List
from tkinter import *
from PIL import Image,ImageTk

from actions import submit
from incident import Direction, Displayer


def generate_option_menu(frame: Frame, options_list: List[str]):
    """
    Generate string option menu from an option list
    :param frame: The frame
    :param options_list: The list of options
    :return: The option menu
    """
    v = StringVar()
    v.set(options_list[0])
    option_menu = OptionMenu(frame, v, *options_list)
    option_menu.grid(sticky="ew")
    return option_menu


def generate_form(frame: Frame):
    """
    Generate the form.

    :param frame: The frame
    :return: The list of (label, input)
    """
    pos = 0
    form = []
    for label_name in Displayer.values():
        label = Label(frame, text=label_name)
        label.grid(row=pos, column=0)
        entry = Entry(frame)
        entry.grid(row=pos, column=2)
        form.append((label_name, entry))
        pos += 1
    btn = ttk.Button(frame, text="Submit", command=lambda: submit(form))
    btn.place(relx=0.5, rely=1.3, anchor=S)
    return form


def display_image(image_path: str):
    """
    Display a new window with the image.

    :param image_path: Relative path to the image
    """
    window = Toplevel()
    window.title("Modelisation result")
    im = Image.open(image_path)
    img = ImageTk.PhotoImage(im)
    lbl = Label(window, image=img)
    lbl.pack()
    window.mainloop()
