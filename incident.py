from datetime import datetime, timezone
from enum import Enum

from mongoengine import *


class Displayer(Enum):
    """
    Enum for the mapping between back and front name
    """
    wind_speed = "Vitesse vent (m/s)"
    wind_direction = "Orientation du vent"
    latitude = "Latitude (en °)"
    longitude = "Longitude (en °)"

    @staticmethod
    def values():
        return [displayer.value for displayer in Displayer]

    @staticmethod
    def from_value(value):
        for displayer in Displayer:
            if displayer.value == value:
                return displayer.name


class Direction(Enum):
    """
    Enum for the direction of the wind
    """
    NN = "NN"
    SS = "SS"
    EE = "EE"
    OO = "OO"
    NE = "NE"
    NO = "NO"
    SE = "SE"
    SO = "SO"

    @staticmethod
    def values():
        return [direction.value for direction in Direction]


class Incident(Document):
    """
    Describe the incident object
    """

    wind_speed = FloatField()
    """
    The wind speed of the incident
    """

    wind_direction = StringField(choices=Direction.values(), default=Direction.NE.value)
    """
    The direction of the wind
    """

    location = GeoPointField()
    """
    The location of the incident
    """

    creation_date = DateField(default=datetime.now(timezone.utc))
    """
    The date of the incident
    """

    @staticmethod
    def from_dict(data: dict):
        """
        Create incident from dict
        """
        return Incident(
            wind_speed=data.get(Displayer.wind_speed.name),
            wind_direction=data.get(Displayer.wind_direction.name),
            location=[float(data.get(Displayer.latitude.name)), float(data.get(Displayer.longitude.name))]
        )

    def __str__(self):
        # Remove this shit
        tmp_dict= {
            "NN": 1,
            "SS": 2,
            "EE": 3,
            "OO": 4,
            "NO": 5,
            "NE": 6,
            "SE": 7,
            "SO": 8,
        }

        return f"{self.location[0]} {self.location[1]} {self.wind_speed} {tmp_dict[self.wind_direction]}"
